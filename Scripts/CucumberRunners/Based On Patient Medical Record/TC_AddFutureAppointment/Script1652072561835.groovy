import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

import based_on_patient_medical_record.CR_AddFutureAppointment_Runner

CucumberKW.runWithCucumberRunner(CR_AddFutureAppointment_Runner.class)
