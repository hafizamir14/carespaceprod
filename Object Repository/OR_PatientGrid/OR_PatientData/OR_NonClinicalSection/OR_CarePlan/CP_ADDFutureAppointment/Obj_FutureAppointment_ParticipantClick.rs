<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_FutureAppointment_ParticipantClick</name>
   <tag></tag>
   <elementGuidId>4a423ce8-dd29-4c68-8a91-446ca9bdc1c9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//td[contains(@data-container-for, 'appointment.participants')]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame</value>
      <webElementGuid>a24e92ad-cbee-4255-9f69-a766af218ec4</webElementGuid>
   </webElementProperties>
</WebElementEntity>
