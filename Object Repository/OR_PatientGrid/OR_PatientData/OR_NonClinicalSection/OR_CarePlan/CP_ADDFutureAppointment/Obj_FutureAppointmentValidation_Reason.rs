<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_FutureAppointmentValidation_Reason</name>
   <tag></tag>
   <elementGuidId>22e5e121-ca2c-4aec-a2ea-b675c9e818e0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//div[contains(@id,'ccmAppointmentsDiv')])//td[contains(text(), 'Confusion')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame</value>
      <webElementGuid>bc960465-56bc-4465-8e19-3121611a4686</webElementGuid>
   </webElementProperties>
</WebElementEntity>
