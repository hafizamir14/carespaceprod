<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_ProcedureResults_ProcedureSelect</name>
   <tag></tag>
   <elementGuidId>bbb56904-610b-45b0-8ce9-ccf40f86bd2c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//strong[contains(text(), 'Anesth, open head surgery')]//parent::li</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@placeholder='Search and select analyte']</value>
      <webElementGuid>f8c63344-c0e0-4ea1-804c-2749d421eaa6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame</value>
      <webElementGuid>6afd0b2d-0629-4a69-91e4-e3b4ea7c19e9</webElementGuid>
   </webElementProperties>
</WebElementEntity>
