<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_ProcedureResults_ReasonSelect</name>
   <tag></tag>
   <elementGuidId>6496e0b3-eb64-476a-8076-b4e67cea46a0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//strong[contains(text(), 'Accidental fall')]//parent::li</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@placeholder='Search and select analyte']</value>
      <webElementGuid>b9bba4aa-6397-40a0-81e2-0e07ce8f3f1a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame</value>
      <webElementGuid>1902e383-8c27-45fb-b0af-63d3d4d6a8c0</webElementGuid>
   </webElementProperties>
</WebElementEntity>
