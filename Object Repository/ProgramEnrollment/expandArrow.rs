<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>expandArrow</name>
   <tag></tag>
   <elementGuidId>c0eba377-89b6-40d8-b3eb-9a1830eb0e9f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id=&quot;collapseArrow&quot;]//span</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/ProgramEnrollment/frameObject</value>
   </webElementProperties>
</WebElementEntity>
