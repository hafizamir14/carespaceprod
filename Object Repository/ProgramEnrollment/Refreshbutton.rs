<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Refreshbutton</name>
   <tag></tag>
   <elementGuidId>ea2f958f-06e6-4732-aa11-1173994eab47</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>//div[@id='tocGrid']//span[@class='k-icon k-i-refresh']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='tocGrid']//span[@class='k-icon k-i-refresh']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/ProgramEnrollment/frameObject</value>
   </webElementProperties>
</WebElementEntity>
