<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>obj_verifyenrolment</name>
   <tag></tag>
   <elementGuidId>27a3ac33-ae12-4435-9d54-a0af7310c663</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>((//div[@id='patient-grid'])//tr)[2]//td[35]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/ProgramEnrollment/frameObject</value>
   </webElementProperties>
</WebElementEntity>
