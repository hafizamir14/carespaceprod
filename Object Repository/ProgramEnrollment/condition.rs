<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>condition</name>
   <tag></tag>
   <elementGuidId>632bc912-f552-4fee-82df-4a5e5c6c62d5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[@aria-owns=&quot;patient_status__input_reason_listbox&quot;]//span//span//span</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/ProgramEnrollment/frameObject</value>
   </webElementProperties>
</WebElementEntity>
