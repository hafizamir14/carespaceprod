<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>obj_closeopendpatient</name>
   <tag></tag>
   <elementGuidId>fa3d6e93-1626-4f5e-8e34-0c05107d3233</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[@class='k-window-title demoGraphicsHeader-program']//parent::div//div[@class='k-window-actions']//a[3]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/ProgramEnrollment/frameObject</value>
   </webElementProperties>
</WebElementEntity>
