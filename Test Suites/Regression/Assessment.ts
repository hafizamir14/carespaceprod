<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Assessment</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>c66c44d3-c592-4fd5-9e07-1d8fa74626f4</testSuiteGuid>
   <testCaseLink>
      <guid>cfba6609-c1cc-4224-8bf5-a812102d7f14</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/Assessments/TC_Assessment_AddNonbillable_OtherAssessment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>01778886-1765-4018-853e-2983edbb3a06</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareGaps/TC_CreateCareGapPWBGoal</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c4c89373-6654-483e-bf40-0bd91f31d64c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareGaps/TC_CreateGaolTaskBoth</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cc58dd40-c9bb-491d-8098-8e9360904191</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareGaps/TC_CareGapStatusEditOpenClosed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>45bd2039-f9d6-49e8-84e8-43e94f74e59f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_EditProcedureOrder</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e85e1955-ba74-4b14-97ab-8b1268184a9f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_DeleteLabOrder</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
