<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Smoke_Cycle1_CareTeam_Execution</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient>zohaiblatif007@gmail.com;mehmood.anjum@solitontechnologies.com;faisal.iqbal@solitontechnologies.com;</mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>37583853-1e35-4fc4-8ede-6019ad80b344</testSuiteGuid>
   <testCaseLink>
      <guid>a10cd732-c634-4032-b79c-3414823a24ae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareTeam/CareManager/TC_AddCareManagerSDatelesstoCDateEDateGreaterToCDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>69b7e5aa-1086-4d59-9b25-22af9edef5dd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareTeam/CareManager/TC_EditCareManagerSDateGreatertoCDateEDateGreaterToCDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>38857d79-ead6-4329-8987-a3dd5916b0c6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareTeam/CareManager/TC_EditCareManagerSDateLesstoCDateEDateEqualToCDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fba6d8c0-cb8d-4e65-becd-39b5ea1838b8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareTeam/CareManager/TC_EditCareManagerSDateLesstoCDateEDateLessToCDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4e8b5d64-71ec-4569-8974-1f7e96630c08</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareTeam/CareManager/TC_DeleteCareManager</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b23c1e5f-70a5-4b33-b287-cc90ecce54b8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareTeam/CarePhysician/TC_AddCarePhysicianSDatelesstoCDateEDateGreaterToCDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ed77ebf7-2bac-4b9a-8921-8f2c96d3c58b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareTeam/CarePhysician/TC_EditCarePhysicianSDateGreatertoCDateEDateGreaterToCDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>390c11b1-a8fd-417e-9e38-5c09746ddc75</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareTeam/CarePhysician/TC_EditCarePhysicianSDateLesstoCDateEDateEqualToCDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5d5e21bc-ca58-4934-9ed0-6ae469931807</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareTeam/CarePhysician/TC_EditCarePhysicianSDateLesstoCDateEDateLessToCDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c8dd01b3-3131-4a0b-9666-37a3fd4e42a6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareTeam/CarePhysician/TC_DeleteCarePhysician</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a55a5609-e9e7-4b1b-a0ca-aa45d1c97c19</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareTeam/Nurse/TC_AddNurseSDatelesstoCDateEDateGreaterToCDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9fcf5e8c-b320-4ee6-8f5d-a84727c89fc4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareTeam/Nurse/TC_EditNurseSDateGreatertoCDateEDateGreaterToCDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3900267c-e6a4-4783-bd72-c92be8763e51</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareTeam/Nurse/TC_EditNurseSDateLesstoCDateEDateEqualToCDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e7a2ec51-a159-497a-a543-0fdf1c5468f4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareTeam/Nurse/TC_EditNurseSDateLesstoCDateEDateLessToCDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>05bd1a4f-9443-4c2b-9f6b-5bdf3acd9057</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareTeam/Nurse/TC_DeleteNurse</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
