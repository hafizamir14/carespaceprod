<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Smoke_Cycle1_VerifySMSFuntionality_Execution</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient>zohaiblatif007@gmail.com;mehmood.anjum@solitontechnologies.com;faisal.iqbal@solitontechnologies.com;</mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>5c2091bd-7c12-4949-b3db-96d0160dd594</testSuiteGuid>
   <testCaseLink>
      <guid>76ca484c-1964-4502-860d-e3daffc87b97</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/SMSFunctioality/TC_CreatAppointmentfromPatientGrid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8fbe5efe-8cf3-4dc8-a4f7-3824a6c00e48</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/SMSFunctioality/TC_VerifySMSfromPatientGridAppointment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>833b89d9-8b12-4c5f-9eb8-68e2d3a8fd60</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/SMSFunctioality/TC_VerifySMSEnglishSchedualAppointment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dce96ac9-fd11-4ab7-ba2c-1c5a5dcf3812</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/SMSFunctioality/TC_VerifySMSspanishSchedualAppointment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>547eb685-72cc-4fb3-b812-f0f779969256</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/SMSFunctioality/TC_VrifysendandResendafterEditMessage</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>944eb4b0-211c-43d1-8a5a-428f47c34bd1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/SMSFunctioality/TC_CreatAppointmentfromTransitionGrid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>53a49092-989f-4072-b6ef-24fa47f34c27</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/SMSFunctioality/TC_VerifySMSfromTransitionGridAppointment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1e59b012-7172-4c2e-8217-7a4a681d955c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/SMSFunctioality/TC_VerifySMSEnglishrecurrenceSchedualAppointment</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
