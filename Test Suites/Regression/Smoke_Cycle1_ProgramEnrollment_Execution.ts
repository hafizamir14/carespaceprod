<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Smoke_Cycle1_ProgramEnrollment_Execution</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient>zohaiblatif007@gmail.com;mehmood.anjum@solitontechnologies.com;faisal.iqbal@solitontechnologies.com;</mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>4581d1fa-b894-4bf0-9c65-1c99fd7f71ce</testSuiteGuid>
   <testCaseLink>
      <guid>dd303037-a6b5-4e31-82a9-a4325d8d323c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ProgramEnrollment/TC_SetProgramEnrollmentSearchpatient</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>38654c4b-ced2-4e6c-a57e-40c6a1443f53</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ProgramEnrollment/TC_SetProgramEnrollmentOpenedPatient</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f72e0c3c-ad94-47a6-8fc9-8511eb9f3596</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ProgramEnrollment/TC_EditProgramEnrollmentOpenedPatient</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8696ec70-8177-488d-b506-25f3ff3e3a52</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ProgramEnrollment/TC_SetProgramEnrollmentTransitionGridOpenedPatient</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>387b2e6e-bfc6-46b0-a35c-723ded9d5cbf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ProgramEnrollment/TC_EditProgramEnrollmentTransitionGridOpenedPatient</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
