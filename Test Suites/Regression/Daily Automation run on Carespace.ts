<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Daily Automation run on Carespace</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>6b8f95ef-8d39-4287-b1c7-6227fc63354f</testSuiteGuid>
   <testCaseLink>
      <guid>2c673749-e61b-4dfe-b3ad-68d40effbe0e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/Assessments/TC_Assessment_AddRecommenedAssessment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>224a13b8-8383-4ab9-b85e-20be3d6e3a0c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/Assessments/TC_Assessment_AddBillable_OtherAssessment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4d45bea5-d592-4841-abdd-5bd509025eac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/Assessments/TC_Assessment_AddNonbillable_OtherAssessment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>680c42ab-dd11-4f21-88ec-61611a86c18a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/Assessments/TC_Assessment_AddCustom</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>094ffc78-5b9b-4933-a872-e2342cc2584c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/Assessments/TC_Assessment_AddBasedonPrevious</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1cd205fd-fac9-45c6-8da8-2f08d939c59e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/Assessments/TC_Assessment_CompareAssessment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b82ed84f-69fc-49b6-b717-99cc2dcc465f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/Assessments/TC_Assessment_Cancelbutton</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>be883db8-a698-489f-b876-d69d63a85b0b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareGaps/TC_CreateTaskNoReccurence</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5e498d5d-7fee-4b10-bcea-96f535d9375c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareGaps/TC_CreateGaolTaskBoth</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5f08153a-bc87-420f-bfab-23c0d911cbb7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareGaps/TC_CreateCareGapPWBGoal</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>825e3c50-8f11-4887-8094-428ed72baaf4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareGaps/TC_CreateCareGapPWBTask</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9949b351-d00d-4527-956d-e2c466eb5911</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareGaps/TC_CareGapValidation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f5980914-fc1c-4f0c-9e18-b04138269198</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareGaps/TC_CareGapStatusEditOpenClosed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fb68def4-d860-454a-9b6a-17e3617a8971</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareGaps/TC_CareGapStatusClosedwithButton</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b388123d-be4b-46d8-9480-fff2f6f004cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareGaps/TC_CareGapExportFile</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b7a530c6-1420-4878-941f-6fd3abcda192</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareTeam/CareManager/TC_AddCareManagerSDatelesstoCDateEDateGreaterToCDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0ead5ba0-9b32-487d-b6f9-873595cf6d3d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareTeam/CareManager/TC_EditCareManagerSDateGreatertoCDateEDateGreaterToCDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2771cb98-d50d-4b33-a392-ba6fab0a5c03</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareTeam/CareManager/TC_EditCareManagerSDateLesstoCDateEDateEqualToCDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>96e717c5-d8a1-4ceb-935c-956813ae344b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareTeam/CareManager/TC_EditCareManagerSDateLesstoCDateEDateLessToCDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>193281e8-8a29-4afb-b686-63eba6caab3c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareTeam/CareManager/TC_DeleteCareManager</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>52430b2b-a7d3-40aa-b46e-03e546c96bd1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareTeam/CarePhysician/TC_AddCarePhysicianSDatelesstoCDateEDateGreaterToCDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9db3067a-2bec-4b4b-9869-82bba6d3dc8a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareTeam/CarePhysician/TC_EditCarePhysicianSDateGreatertoCDateEDateGreaterToCDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>42860f09-06f3-457f-8428-089acc3eeeb7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareTeam/CarePhysician/TC_EditCarePhysicianSDateLesstoCDateEDateEqualToCDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ce003b7b-9e65-42c5-b35c-5df41fc0f019</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareTeam/CarePhysician/TC_EditCarePhysicianSDateLesstoCDateEDateLessToCDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c89182ff-32a5-465a-a045-7070db913491</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareTeam/CarePhysician/TC_DeleteCarePhysician</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>75ff60bd-a5f0-4ee3-bff3-72ed57e0c950</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareTeam/Nurse/TC_AddNurseSDatelesstoCDateEDateGreaterToCDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5ccd798c-838d-43b0-a0bd-01a23e20bab8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareTeam/Nurse/TC_EditNurseSDateGreatertoCDateEDateGreaterToCDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1c470a75-ddb4-49c9-9df4-cf2f2cf3e54a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareTeam/Nurse/TC_EditNurseSDateLesstoCDateEDateEqualToCDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0ca13a0c-13c4-45b8-a964-d6e986f0d3f1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareTeam/Nurse/TC_EditNurseSDateLesstoCDateEDateLessToCDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>94d73d5c-2d4c-4cc8-aab3-a6bc3f239875</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/CareTeam/Nurse/TC_DeleteNurse</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2a2aa2a3-182a-45b6-8dc3-a5af9ad6b7f5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_AddProblem</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>13d7fb7d-acfa-4b85-8663-88874d8439a6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_AddAllergies</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3ccb36c3-be45-4231-86cb-5c3293004f45</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_AddFamilyHistory</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>18734597-4405-47bf-87f1-0547632b2515</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_AddFunctionalStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>63de0516-c4ac-42b7-b17b-a737fd545860</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_AddImagingOrder</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>beda66da-e2b6-4753-9f28-088d940d8bd1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_AddImagingResults</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6aabdd7d-4fbd-49da-adca-b5f58adf7e7a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_AddImmunization</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ffa8d321-3754-42ea-9309-c1a158c66a63</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_AddLabOrder</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b395a85f-1af1-4b3d-8033-9a3f85826860</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_AddLabResults</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d1d00487-d148-4a1d-aa85-e1209eeb3427</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_AddMedication</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cdee4fd0-ea64-4a78-bc01-c31f85ef645b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_AddMentalStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1ecc1ec4-e0c8-4032-a0d0-c7b5ddedb34a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_AddProcedureOrder</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d211af9b-31a3-4f78-b216-6811adc68c5c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_AddProcedureResults</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4a690676-b1e0-4ad8-80df-a673f604684b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_AddSocialHistory</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a5ab7ed1-0348-4fe2-98d7-8f65934666c8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_AddVitalSigns</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>456800df-753e-43e6-b325-791c41df1103</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_EditProblem</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b0de1ed2-3322-40f6-8524-4d6743b6840f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_EditAllergies</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3c7e2495-a813-4aa0-bda0-43e6f4ef510f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_EditFamilyHistory</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>90cac375-8e0a-411d-9053-b3eec72d65bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_EditFunctionalStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>466b4a68-155b-4227-bcfe-8a9a72471624</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_EditImagingOrder</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b72b3299-5cac-42cd-bc3e-9b7fa3a77d72</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_EditImagingResults</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>649876bb-c02d-495e-8d71-1e27bb997c77</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_EditImmunization</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9ef76332-1ce8-4041-97de-41dab7d61972</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_EditLabOrder</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2e3c5cf1-4898-4b67-911d-a29abd1a9aa6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_EditLabResults</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>50c94dee-5d36-4798-8bfa-1c44897fe039</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_EditMedication</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cfb7ac22-362b-428f-947a-f27bcc44bd82</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_EditMentalStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c03e9a32-eace-4aab-83dd-74ea5c34d591</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_EditProcedureOrder</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c450016-1870-4e3a-90eb-88b085513a62</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_EditProcedureResults</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9d652c53-3e62-4c3f-a765-53004ba98df3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_EditSocialHistory</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>736a7a73-9541-44cd-9362-ae96fa72b6f3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_EditVitalSigns</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>780f0a2d-6c2f-4c9b-8f68-55cd82938dbf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_DeleteProblem</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>882cbdfa-9e7c-48c9-a7a0-e9868ca56831</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_DeleteAllergies</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9286a8e6-1978-4a2d-b50d-bcd724a66f19</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_DeleteFamilyHistory</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>20158fd3-696f-4a7e-9381-78b6ed0bf619</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_DeleteFunctionalStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b32a0c77-b998-4e3b-9bfd-4e1ca1f64d72</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_DeleteImagingOrder</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fd3e0d16-621c-47ef-8db1-946d75697ad5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_DeleteImagingResults</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c47d785a-5b4b-4023-8bfb-117c26832f63</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_DeleteImmunization</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e4d0d03f-811e-4081-89a8-73665c157299</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_DeleteLabOrder</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>617de8d6-ace7-43ce-9892-83497058347f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_DeleteLabResults</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>918bd0e4-7f34-40ff-a9f7-af52cd86915d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_DeleteMedication</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c734d115-b624-459d-97eb-032384f6347c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_DeleteMentalStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>79b83d1f-fdf8-4bd0-b359-0a603c33693d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_DeleteProcedureOrder</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3dec6f4b-a4fa-4aa8-a517-9c5fe08b5fc5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_DeleteProcedureResults</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>03072208-dfc9-4849-b47e-6a4c8da3feb4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_DeleteSocialHistory</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ed5c2fb1-1ff4-4e5d-b268-d422641f415c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ClinicalSection/TC_DeleteVitalSigns</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>71173943-3039-4f24-9781-cf1f92624256</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/Contacts/TC_AddGuardian</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c04d0fa2-8948-4b3a-ab29-2eca3540a8ec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/Contacts/TC_EditGuardian</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>81e0c8f5-f2b9-4bbf-9587-a3eeed105ccd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/Contacts/TC_DeleteGuardian</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3ed6ecc4-f544-44d6-aa77-045cea6a8bc6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/Contacts/TC_AddSupportPerson</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>374b52ec-35ca-46f5-943a-9f23fd209a4a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/Contacts/TC_EditSupportPerson</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>06970679-3402-40f7-bf03-050f4f1bad28</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/Contacts/TC_DeleteSupportPerson</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>75b6497b-428b-4250-be48-54b838c0ea04</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/Documentationfunctionality/TC_UploadDocument</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>71ad1a05-019c-49ef-b01c-24ef21b02964</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/Documentationfunctionality/TC_UpdateDocument</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>81fc7c52-8d3a-412a-9aff-dd8ff83ff9f4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/Documentationfunctionality/TC_DeleteDocument</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>19a53ceb-2b2c-4c89-a3a2-acfead77784b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/Encounter/TC_AddEncounter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>25c3ddbb-74b9-48c9-927c-85d2b879e9bd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/Encounter/TC_EditEncounter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9aaa4f5a-7434-43d4-bf4b-1846b423e9a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/Encounter/TC_DeleteEncounter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>719623b5-28b8-49c9-a735-b9bb70ba9ce7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/Insurance/TC_AddInsurance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3bc2200b-2471-4d51-bcc6-5cd3a8568a96</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/Insurance/TC_EditInsurance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>df910db2-469d-4827-aefb-7e3ad06f0c10</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/Insurance/TC_DeleteInsurance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>672f24fc-6b99-4986-89db-5b90779843c2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/LeftFiltersPatientGrid/TC_UMP_Facility</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6463b801-aef1-4aaa-9929-f10ce940662d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/LeftFiltersPatientGrid/TC_UMP_MultiFacility</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>842cb0d5-c4ea-4216-ad5c-dbc3f416102a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/LeftFiltersPatientGrid/TC_UMP_Provider</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5795734c-0c1a-4207-b42b-3ae3f5dde845</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/LeftFiltersPatientGrid/TC_M_Program</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>90c072b9-b5d1-46ca-b454-9565d9fc1e90</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/LeftFiltersPatientGrid/TC_M_Program_Enrollment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0bbcd48f-c6b5-49c8-86d2-742507714aad</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/LeftFiltersPatientGrid/TC_MP_HCCScore</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0691a998-9562-41e8-a779-f4e5c091991d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/PatientDemographics/TC_CreateNewPatient</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b67014bb-c031-44a0-821c-c6fe7c6e7318</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/PatientDemographics/TC_EditPatientdemographicRecord</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b0eeaa37-0a12-4166-988c-7f06dd1e73f1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/PatientDemographics/TC_EmailSendWhenEmailNotSet</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d72fdb53-0312-4db8-ac04-c3ef60cd2d49</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/PatientDemographics/TC_EmailSendWhenEmailSet</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2dc5c575-628a-4e10-aaae-8efd10694609</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/PatientDemographics/TC_ExportPatientRecordFile</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e15543d7-0313-4daa-8ed6-995c0cff077a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/PatientDemographics/TC_EmailResend</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2378f28d-e844-4dc2-b854-ac3a17f18857</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/PatientDemographics/TC_UploadPatientPic</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c6d6ec0b-97a0-4d1d-9368-87eb1ce8bd64</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/PatientDemographics/TC_DeletePatientPic</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>38409862-3ac4-48f2-8fe7-5915ef41401f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ProgramEnrollment/TC_SetProgramEnrollmentSearchpatient</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>51473948-27a6-402c-b085-bf43e3bce0e2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ProgramEnrollment/TC_SetProgramEnrollmentOpenedPatient</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6c231f42-6840-46f6-8c55-b1abc9ca7038</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/ProgramEnrollment/TC_EditProgramEnrollmentOpenedPatient</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>01fe233c-2221-4eab-bea8-a6ee69fd95f1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/SMSFunctioality/TC_CreatAppointmentfromPatientGrid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dee5fc8c-a384-4f18-b696-45e54bfb8672</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/SMSFunctioality/TC_VerifySMSfromPatientGridAppointment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6dab1572-bc84-47e5-888c-86d282e6d972</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/SMSFunctioality/TC_VerifySMSEnglishSchedualAppointment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0c212ca6-d514-476b-80d4-3115cdfc0c81</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/SMSFunctioality/TC_VerifySMSspanishSchedualAppointment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3f5964fd-d14b-430e-b9d1-1e29d9caf810</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CucumberRunnerClasses/SMSFunctioality/TC_VrifysendandResendafterEditMessage</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
