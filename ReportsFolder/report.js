$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Include/features/PatientGrid/ClinicalSection/LabOrder.feature");
formatter.feature({
  "name": "Add Edit and Delete in Lab Order",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@LabOrders"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Add in lab orders with all fields",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@AddLabOrder"
    },
    {
      "name": "@Regression"
    },
    {
      "name": "@Sanity"
    }
  ]
});
formatter.step({
  "name": "I search \u003cPatient\u003e using global search",
  "keyword": "Given "
});
formatter.step({
  "name": "I click on Clinical tab",
  "keyword": "And "
});
formatter.step({
  "name": "I click on Lab Orders\ttab",
  "keyword": "* "
});
formatter.step({
  "name": "I click on lab orders Add button",
  "keyword": "When "
});
formatter.step({
  "name": "I enter \u003cLaboratary\u003e in lab order laboratory field",
  "keyword": "And "
});
formatter.step({
  "name": "I enter \u003cOrder_Provider\u003e in lab order order provider field",
  "keyword": "* "
});
formatter.step({
  "name": "I enter \u003cDate\u003e in lab orders date field",
  "keyword": "* "
});
formatter.step({
  "name": "I add \u003cPanel\u003e in lab order",
  "keyword": "* "
});
formatter.step({
  "name": "I enter \u003cSTAT\u003e in lab orders stat field",
  "keyword": "* "
});
formatter.step({
  "name": "I enter \u003cDiagnosis\u003e in lab order diagnosis field",
  "keyword": "* "
});
formatter.step({
  "name": "I click on lab order specimen drop down",
  "keyword": "* "
});
formatter.step({
  "name": "I enter \u003cCollection_Date\u003e in lab order collection date field",
  "keyword": "* "
});
formatter.step({
  "name": "I enter \u003cType\u003e in lab order type field",
  "keyword": "* "
});
formatter.step({
  "name": "I enter \u003cSpecimen_No\u003e in lab order specimen no. field",
  "keyword": "* "
});
formatter.step({
  "name": "I enter \u003cNotes\u003e in lab order notes field",
  "keyword": "* "
});
formatter.step({
  "name": "I click on lab order Save All button",
  "keyword": "* "
});
formatter.step({
  "name": "I should see success message for added lab order",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Patient",
        "Laboratary",
        "Order_Provider",
        "Date",
        "Panel",
        "STAT",
        "Diagnosis",
        "Collection_Date",
        "Type",
        "Specimen_No",
        "Notes"
      ]
    },
    {
      "cells": [
        "ISAACS, KRISTEN",
        "VPA Labs",
        "Latif, Zohaib",
        "01/28/2021 13:57",
        "Lipid Panel",
        "Yes",
        "(Cholera, unspecified)",
        "01/28/2021 13:57",
        "Serum",
        "10",
        "Lab order added"
      ]
    }
  ]
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I navigate to patient grid",
  "keyword": "Given "
});
formatter.match({
  "location": "SD_PatientGrid.I_navigate_to_patient_grid()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Add in lab orders with all fields",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@LabOrders"
    },
    {
      "name": "@AddLabOrder"
    },
    {
      "name": "@Regression"
    },
    {
      "name": "@Sanity"
    }
  ]
});
formatter.step({
  "name": "I search ISAACS, KRISTEN using global search",
  "keyword": "Given "
});
formatter.match({
  "location": "SD_SearchPatient.search_Patient(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on Clinical tab",
  "keyword": "And "
});
formatter.match({
  "location": "ProblemListsteps.I_am_on_PWB_CLinical_section()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on Lab Orders\ttab",
  "keyword": "* "
});
formatter.match({
  "location": "Labordersteps.I_click_on_laborder_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on lab orders Add button",
  "keyword": "When "
});
formatter.match({
  "location": "Labordersteps.I_click_on_add_button_to_add_lab_order()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter VPA Labs in lab order laboratory field",
  "keyword": "And "
});
formatter.match({
  "location": "Labordersteps.I_select_laboratary_from_dropdown(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter Latif, Zohaib in lab order order provider field",
  "keyword": "* "
});
formatter.match({
  "location": "Labordersteps.select_orderprovider_from_dropdown(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter 01/28/2021 13:57 in lab orders date field",
  "keyword": "* "
});
formatter.match({
  "location": "Labordersteps.I_enter_date_for_order_in_lab(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I add Lipid Panel in lab order",
  "keyword": "* "
});
formatter.match({
  "location": "Labordersteps.I_add_panel_from_add_panel_for_laborder(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter Yes in lab orders stat field",
  "keyword": "* "
});
formatter.match({
  "location": "Labordersteps.STAT_from_checkbox_for_laborder(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter (Cholera, unspecified) in lab order diagnosis field",
  "keyword": "* "
});
formatter.match({
  "location": "Labordersteps.I_set_the_diagnoses_for_laborder(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on lab order specimen drop down",
  "keyword": "* "
});
formatter.match({
  "location": "Labordersteps.specimen_template_for_laborder()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter 01/28/2021 13:57 in lab order collection date field",
  "keyword": "* "
});
formatter.match({
  "location": "Labordersteps.collection_date_for_lab_order(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter Serum in lab order type field",
  "keyword": "* "
});
formatter.match({
  "location": "Labordersteps.specimen_type_for_lab_order(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter 10 in lab order specimen no. field",
  "keyword": "* "
});
formatter.match({
  "location": "Labordersteps.specimen_number_for_lab_order(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter Lab order added in lab order notes field",
  "keyword": "* "
});
formatter.match({
  "location": "Labordersteps.notes_for_laborder(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on lab order Save All button",
  "keyword": "* "
});
formatter.match({
  "location": "Labordersteps.saveall_button_to_save_lab_order_record()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should see success message for added lab order",
  "keyword": "Then "
});
formatter.match({
  "location": "Labordersteps.saved_successfully_alert_popup_for_laborder()"
});
formatter.result({
  "status": "passed"
});
});