package patientGrid
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.And


class SD_SoapNote_Provider1_1 {


	@And("I select Render Provider")
	public void SelectRender1_1() {

		WebUI.waitForElementClickable(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/SoapNotes/patient_Info/Obj_RenderingProvider_Click'), 15)

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/SoapNotes/patient_Info/Obj_RenderingProvider_Click'))
		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/SoapNotes/ProviderFacility_1_1/Obj_RenderingProviderSelect'))
	}

	@And("I verify (.*) as provider facility")
	public void SelectFaclity1_1(String Facility) {

		String Actual_Facility = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/SoapNotes/ProviderFacility_1_1/Obj_Facility1_1Validation'))

		if(!Actual_Facility.contains(Facility)){

			WebUI.verifyMatch(Actual_Facility, Facility, false)
		}
	}
	@And("I verify (.*) Rending Provider")
	public void SelectRendering1_1(String Rendering) {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/SoapNotes/patient_Info/Obj_RenderingProvider_Click'))
		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/SoapNotes/patient_Info/Obj_RenderingProvider_Select'))


		Thread.sleep(1000)

		String Actual_Rend = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/SoapNotes/ProviderFacility_1_1/Obj_Rendering1_1Validation'))

		if(!Actual_Rend.contains(Rendering)){

			WebUI.verifyMatch(Actual_Rend, Rendering, false)
		}
	}
}