package patientGrid
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.And
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


class SD_AdvanceSearch_SuperBill {


	@And("I drag first slider")
	public void Slider1() {


		WebUI.dragAndDropByOffset(findTestObject("Object Repository/OR_PatientGrid/SuperBill_AdvanceSearch/Obj_Slider1"), 10, 0)
		Thread.sleep(4000)
	}

	@And("I drag second slider")
	public void Slider2() {


		WebUI.dragAndDropByOffset(findTestObject("Object Repository/OR_PatientGrid/SuperBill_AdvanceSearch/Obj_Slider2"), 30, 10)
		Thread.sleep(12000)
	}

	@And("I click on slider search button")
	public void SliderSearchBTN() {


		WebUI.click(findTestObject("Object Repository/OR_PatientGrid/SuperBill_AdvanceSearch/Obj_SliderSearchBTN"))
		Thread.sleep(6000)
	}

	@And("I verify (.*) as slider filter is working fine")
	public void SliderFilterVerification(String SliderProvider) {


		WebUI.click(findTestObject("Object Repository/OR_PatientGrid/SuperBill_AdvanceSearch/Obj_SliderProviderClick"))
		Thread.sleep(4000)

		WebUI.setText(findTestObject("Object Repository/OR_PatientGrid/SuperBill_AdvanceSearch/Obj_SliderProvider_Input"), SliderProvider)
		Thread.sleep(3000)
		String Actual_SliderProvider = WebUI.getText(findTestObject('Object Repository/OR_PatientGrid/SuperBill_AdvanceSearch/Obj_SliderProviderSelect'))

		if(!Actual_SliderProvider.contains(SliderProvider)){

			WebUI.verifyMatch(Actual_SliderProvider, SliderProvider, false)
		}
	}

	@And("I select (.*) as slider filter provider")
	public void SelectSliderProvider(String SliderProvider) {

		WebUI.click(findTestObject("Object Repository/OR_PatientGrid/SuperBill_AdvanceSearch/Obj_SliderProviderClick"))
		Thread.sleep(2000)
		WebUI.setText(findTestObject("Object Repository/OR_PatientGrid/SuperBill_AdvanceSearch/Obj_SliderProvider_Input"), SliderProvider)
		Thread.sleep(1000)
		WebUI.click(findTestObject("Object Repository/OR_PatientGrid/SuperBill_AdvanceSearch/Obj_SliderProviderSelect"))
		WebUI.scrollToElement(findTestObject("Object Repository/OR_PatientGrid/SupperBill/Obj_FromDate_Input"), 0)
	}

	@And("I click on advance search link")
	public void AdvSearchLink() {


		WebUI.click(findTestObject("Object Repository/OR_PatientGrid/SuperBill_AdvanceSearch/Obj_AdvSearchLink"))
		Thread.sleep(6000)
	}

	@Then("I should see (.*) as title")
	public void VerifyTitleAdvSearch(String AdvTitle) {

		String actualTitle = WebUI.getText(findTestObject("Object Repository/OR_PatientGrid/SuperBill_AdvanceSearch/Obj_AdvanceSearchTitle"))
		WebUI.verifyEqual(actualTitle, AdvTitle)
	}

	@When("I checked range checkbox")
	public void RangeCheckbox() {

		WebUI.click(findTestObject("Object Repository/OR_PatientGrid/SuperBill_AdvanceSearch/Obj_RangeCheckBox"))
	}

	@And("I drag adv search slider")
	public void Slider3() {


		WebUI.dragAndDropByOffset(findTestObject("Object Repository/OR_PatientGrid/SuperBill_AdvanceSearch/Obj_Slider3"), 10, 0)
		Thread.sleep(2000)
	}

	@And("I click on advance search button")
	public void AdvanceSearchBTN() {


		WebUI.click(findTestObject("Object Repository/OR_PatientGrid/SuperBill_AdvanceSearch/Obj_AdvanceSearchBTN"))
		Thread.sleep(2000)
	}

	@Then("I verify that search button is disabled")
	public void SearchBtnDisabled() {

		WebUI.verifyElementNotClickable(findTestObject("Object Repository/OR_PatientGrid/SuperBill_AdvanceSearch/Obj_SliderSearchBTN"))
		Thread.sleep(2000)
	}

	@When("I checked firstname checkbox")
	public void FirstNameCheckbox() {


		WebUI.click(findTestObject("Object Repository/OR_PatientGrid/SuperBill_AdvanceSearch/Obj_FNCheckBox"))
		Thread.sleep(2000)
	}

	@When("I checked startsWith checkbox")
	public void StartsWithCheckbox() {


		WebUI.click(findTestObject("Object Repository/OR_PatientGrid/SuperBill_AdvanceSearch/Obj_StartsWithCheckBox"))
		Thread.sleep(2000)
	}
	@And("I enter (.*) as startsWith filters")
	public void EnterStarWithKeyword(String Keywords) {

		WebUI.setText(findTestObject("Object Repository/OR_PatientGrid/SuperBill_AdvanceSearch/Obj_StartsWith_Input"), Keywords)
		Thread.sleep(2000)
	}
}