package patientGrid

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.aspectj.asm.internal.ProgramElement
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver

import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import groovypackage.Methods

public class SD_IPAdmission_ERVisit_Transition {

	
	@Then("I should see (.*) on the patient grid as Columns")
	public void I_should_see_ERVisit_filters(String Columns) {


		WebUI.scrollToElement(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_VerifyERVisitScroll'),5)

		"ER Visits Verify"
		String Actual_Col = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_VerifyERVisit'))
		WebUI.verifyMatch(Actual_Col, Columns, false)

	}

	@Then("I should see (.*) on the patient grid as Column")
	public void I_should_see_IPAdmission_filters(String Columns) {


		WebUI.scrollToElement(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_VerifyIPAdmissionScroll'),5)

		"IP Admission Verify"
		String Actual_Col2 = WebUI.getText(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_VerifyIPAdmission'))
		WebUI.verifyMatch(Actual_Col2, Columns, false)




	}

	@Then("I should be able to (.*) this column")
	void open_Care_Coordination_SortingERVisit(String Sort) {

		WebUI.scrollToElement(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_VerifyIPAdmissionScroll'),5)
		
		Thread.sleep(2000)

		'click on arrow'
		WebUI.waitForElementPresent(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_ERVisitArrowClick'), 0)
		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_ERVisitArrowClick'))
		'Dscending'
		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_ERVisit_SortDsc'))

		Thread.sleep(2000)

		WebUI.scrollToElement(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_VerifyIPAdmissionScroll'),5)
		
		'click on arrow'
		WebUI.waitForElementPresent(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_ERVisitArrowClick'), 0)
		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_ERVisitArrowClick'))
		'Ascending'
		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_ERVisit_SortAsc'))

		Thread.sleep(2000)
	}

	@Then("I should be able to (.*) this columns")
	void open_Care_Coordination_SortingIPAdmission(String Sort) {

		WebUI.scrollToElement(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_VerifyIPAdmissionScroll'),5)
		
		
		Thread.sleep(2000)

		'click on arrow'
		WebUI.waitForElementPresent(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_IPAdmissionArrowClick'), 0)
		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_IPAdmissionArrowClick'))
		'Dscending'
		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_IPAdmission_SortDsc'))

		Thread.sleep(2000)

		WebUI.scrollToElement(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_VerifyIPAdmissionScroll'),5)
		
		'click on arrow'
		WebUI.waitForElementPresent(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_IPAdmissionArrowClick'), 0)
		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_IPAdmissionArrowClick'))
		'Ascending'
		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_IPAdmission_SortAsc'))

		Thread.sleep(2000)
	}


	@And("I enter (.*) as assign date of transition grid")
	void open_Care_Coordination_Date(String Date) {

		Thread.sleep(4000)

		WebUI.setText(findTestObject("Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_Transitioni_AssignDate"), Date)
		Thread.sleep(2000)
	}


		@And("I filter using (.*) on transition IP Admission grid")
		void open_Care_Coordination_IP(String Filters) {
			
			WebUI.scrollToElement(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_VerifyIPAdmissionScroll'),5)
				
			Thread.sleep(2000)
	
			'click on ER Visits Arrow'
			WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_IPAdmissionArrowClick'))
			
			'move to the ER Visits filter label'
			Thread.sleep(2000)
			WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_IP_FilterLabel'))
	
			'Input the patient name'
			Thread.sleep(4000)
			WebUI.clearText(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_IP_Filter_Input'))
	
			WebUI.sendKeys(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_IP_Filter_Input'),Filters)
	
			'Click on filter button'
			WebUI.click(findTestObject('Object Repository/OR_OpenPatient/filterbutton'))
	
			Thread.sleep(2000)
		}

		@And("I filter using (.*) on transition ER Visits grid")
		void open_Care_Coordination_ERVisit(String Filters) {
			
			WebUI.scrollToElement(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_VerifyIPAdmissionScroll'),5)
				
			Thread.sleep(2000)
	
			'click on ER Visits Arrow'
			WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_ERVisitArrowClick'))
			
			'move to the ER Visits filter label'
			Thread.sleep(2000)
			WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_ERVisit_FilterLabel'))
	
			'Input the patient name'
			Thread.sleep(4000)
			WebUI.clearText(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_ERVisit_Filter_Input'))
	
			WebUI.sendKeys(findTestObject('Object Repository/CareCordination_LeftFilters/IPAdmission_ERVisit/Obj_ERVisit_Filter_Input'),Filters)
	
			'Click on filter button'
			WebUI.click(findTestObject('Object Repository/OR_OpenPatient/filterbutton'))
	
			Thread.sleep(2000)
		}


}
