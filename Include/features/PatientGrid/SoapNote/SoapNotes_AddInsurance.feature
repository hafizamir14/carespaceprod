Feature: Soap Note Creation from Schedule flow

	Background: 
		Given I navigate to patient grid

	@Smoke_USMM_CreateInsurance
	Scenario Outline: Add new row in insurance section of patient with all fields
		Given I search <Patient> using global search
		 When I click on Insurance tab to land on insurance section
		  And I click on add insurance button
		    * I enter <Payer> in insurance payer field
		    * I enter <Streetaddress> in insurance street address field
		    * I enter <Cityaddress> in insurance city address field
		    * I enter <Stateaddress> in insurance state address field
		    * I enter <Zipaddress> in insurance zip address field
		    * I enter <Countryaddress> in insurance country address field
		    * I enter <Phone> in insurance phone field
		    * I enter <Policytype> in insurance policy type field
		    * I enter <Insurancerank> in insurance insurance rank field
		    * I enter <Copay> in insurance copay field
		    * I enter <SubscriberId> in insurance subscriber id field
		    * I enter <Relationship> in insurance relationship field
		    * I enter <GroupId> in insurance group id field
		    * I enter <FinancialResParty> in insurance financial res party field
		    * I click on save button to save insurance data
		    * I click on schedule tab
		    * I click on three dots
		    * I click on edit soapnotes
		    * I click on insurance existing button
		    * I checked the insurance checkbox
		    * I click on procced button
		 Then I should see <Insurancerank> and <Payer> and <Policytype> as insurance data in soap note grid

		Examples: 
			| Patient          | Payer | Streetaddress | Cityaddress | Stateaddress | Zipaddress | Countryaddress | Phone        | Policytype | Insurancerank | Copay | SubscriberId | Relationship     | GroupId | FinancialResParty | EffectiveFromdate | EffectiveTodate |
			| Dermo505, Mac505 | AETNA | Thokar        | Lahore      | punjab       |     054422 | Pakistan       | 015846377277 | AARP       | Primary       |    21 |        34848 | Family dependent |      12 | Affiliate         |          03142021 |        04122021 |

	@Smoke_USMM_DeleteInsurance
	Scenario Outline: Delete the row in insurance section of patient
		Given I search <Patient> using global search
		 When I click on encounter tab button to land on enconter section
		  And I click on deleteButton to delete soap note frome encounter
		    * I click on Insurance tab to land on insurance section
		    * I click on deleteButton to delete insurance
		 Then I should see success message for deleted insurance record

		Examples: 
			| Patient          |
			| Dermo505, Mac505 |
