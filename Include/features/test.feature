@CareGap
Feature: Functionality of Care Gap section of a patient

  Background:
    Given I navigate to patient grid
  
  @CreateNewPatient
   @Regression
   @Sanity
    Scenario Outline: Create new patient with all fields
    
	 When I click on patient create button
	  And I enter <MRN> in patient mrn field
		*   I enter <First_Name> in patient first name field
		*   I enter <Midddle_Name> in patient middle name field
		*   I enter <Last_Name> in patient last name field
		*   I enter <Suffix> in patient suffix field
		*   I enter <Birth_Name> in patient birth name field
		*   I enter <Gender> in patient gender field
		*   I enter <DOB> in patient dob field
		*   I enter <Gender_Identity> in patient gender identity field
		*   I enter <Sexual_Orientation> in patient sexual orientation field
		*   I enter <Bith_Address> in patient birth address field
		*   I enter <Bith_State> in patient birth state field
		*   I enter <Bith_Country> in patient birth country field
		*   I enter <Marital_Status> in patient marital status field
		*   I enter <Language> in patient language field
	  *   I enter <Race> in patient race field
		*   I enter <Detailed_Race> in patient detailed race field
		*   I enter <Ethnicity> in patient ethnicity field
		*   I enter <Detailed_Ethnicity> in patient detailed ethnicity field
		*   I enter <Religion> in patient religion field
		*   I enter <Facility> in patient facility field
		*   I enter <Street_Address> in patient street address field
		*   I enter <City_Address> in patient city address field
		*   I enter <State_Address> in patient state address field
		*   I enter <ZIP_Address> in patient zip address field
		*   I enter <Country_Address> in patient country address field
		*   I enter <Home_Phone> in patient home phone field
		*   I enter <Work_Phone> in patient work phone field
		*   I enter <Mobile_Phone> in patient mobile phone field
		*   I enter <Preffered_Phone> in patient preffered phone field
		*   I enter <Email> in patient email field
		*   I enter <Comments> in patient comments field
    *   I click on patient save button
                   #***********   I click on patient proceed button button
	 #Then I should see success message for created patient
                      #**********************		And I should see the newly created patient on grid
    
    Examples:
    | MRN |First_Name|Midddle_Name|Last_Name|Suffix|Birth_Name|Gender|   DOB  |Gender_Identity|   Sexual_Orientation   |Bith_Address|Bith_State|Bith_Country|Marital_Status|Language|Race |Detailed_Race|    Ethnicity     |Detailed_Ethnicity|Religion|   Facility     |Street_Address| City_Address|State_Address|ZIP_Address|Country_Address|Home_Phone|Work_Phone|Mobile_Phone|Preffered_Phone|   Email   |    Comments   |
    |Robeb|   anar67   |   ahi77   |  K29ri   |  Mr  |  Wikllian |Male|12032020|     Female    |Straight or Heterosexual|   Lahore   | Punjab   |  Pakistan  |    Married   |English |Asian|  Pakistani  |Hispanic or Latino|    Asturian      | Islam  |Support Test Fac| Thiokar niaz | north punjab|   Punjab    |    0123   |  Near Iran    |0139848973|0174888888|013344333333|       Home    |test@ss.com|add new patient|   
    
#